# RabbitMQ

This message broker was chosen as it is very popular and well documented software. Actually, the choice was based on its very simple installation and my modest experience of administrating this software. There was no need in imlementing complex logic so basically it could be any message broker.

## Prerequisites

Docker and Docker-Compose need to be installed

## Configuration

* Define the following environmental variables

```
RABBITMQ_DEFAULT_USER: ""
RABBITMQ_DEFAULT_PASS: ""
```

* Create directory named "data"

* When container will be started, go to http://localhost:15672, authorize with defined admin credentials and create a virtualhost and a user for the scripts (optional)

## Usage

Go to the directory with the docker-compose file and run

```
docker-compose up -d
```